

/**
 *
 * @author Jason
 */
public class SumOfPrimes {
	public static void main (String args[]) {
		
		long sum = 0;
		int counter = 1;
		
		for (long i = 1; counter <= 1000 ; i++)
		{
			//System.out.print("Testing " + i + ": ");
			if (isPrime(i))
			{
				//System.out.println("PRIME #" + counter);
				sum = sum + i;
				counter ++;
			} 
		}
		System.out.println(sum);
	}
	public static boolean isPrime(long n) {
		if (n <= 3) {
			//simple test for bottom 3 numbers
			return (n > 1);
		} else if (n % 2 == 0 || n % 3 == 0) {
			//secnd simple test for even numbers or multiples of 3
			//before we hit the big brute force loop
			return false;
		} else {
			//simple tests failed, on to testing everything
			for (int i = 5; i * i <= n; i += 6) {
				if (n % i == 0 || n % (i + 2) == 0) {
					return false;
				}
			}
		return true;
		} // end of isPrime method
	}
}
