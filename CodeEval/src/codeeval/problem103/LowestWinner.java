


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class LowestWinner {
    public static void main (String[] args) throws IOException {

		File file = new File(args[0]);
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String line;
        
		//loop on the input file
		while ((line = buffer.readLine()) != null) {
            line = line.trim();
            // Process line of input Here
    		String plays[] = line.split(" ");
            ArrayList<String> uniquePlays = new ArrayList<>();
    		ArrayList<String> duplicatePlays = new ArrayList<>();
    		
			
			//System.out.println("Processing the plays...");
			// <editor-fold defaultstate="collapsed" >
			for (int i = 0 ; i < plays.length ; i++)
			{
				//System.out.print("Play #" + i + " : " + input[i] + ". ");

				//does the uniques list contain this?
				if(uniquePlays.contains(plays[i]))
					{// this is a duplicate entry

						//System.out.println("This is already in the unique list.");
						if (!duplicatePlays.contains(plays[i]))
							duplicatePlays.add(plays[i]);
						uniquePlays.remove(plays[i]);
				} else {
					//this is a UNIQUE entry so far
					if(duplicatePlays.contains(plays[i]))
						{
						//already in the dupes list do nothing
						//System.out.println("Already in dupes, so do NOT add to uniques...");
					} else {
							uniquePlays.add(plays[i]);
						}
					}
			}// </editor-fold>
			// end of loop processing a single game

    
    		
    		//System.out.println("Sorting the list of unique plays.");
    		Collections.sort(uniquePlays);
    		if (!uniquePlays.isEmpty())
			{
				int lowest = Integer.parseInt(uniquePlays.get(0));
				int player = 0;
				for (int i = 0 ; i < plays.length ; i++)
				{
					if(lowest == Integer.parseInt(plays[i]))
					{
						player = i + 1;
						//System.out.println("Lowest unique guess is " + lowest + " by player #" + i);
					} // end printing the winner
				} // end looping through all unique plays

				System.out.println(player);
			} else {
				//we have a non-unique game. all entries duplicated.
				System.out.println("0");
			}
        } // end of performing loop on file inputs
    } // end of main(String args[])
} // end of main class


