/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codeeval.problem2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Jason
 */
public class LongestLines {
	public static void main (String args[]) throws IOException
	{
		
		File file = new File(args[0]);
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        ArrayList<String> lines = new ArrayList<String>();
		
		String line; //temp string for each new line

		//grab the number of lines to print out
		int number = Integer.parseInt(buffer.readLine());
		//loop on the input file
		while ((line = buffer.readLine()) != null)
		{
            // Process lines of input Here
			lines.add(line);
			
		} // finished reading in the file
		
		//lets spit out the whole file
		//System.out.println("\nPrinting the whole unsorted list of lines:");
		/*
		Iterator i = lines.iterator();
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
		*/
		//time to order the whole file largest to smallest
		//System.out.println("\nTime to sort the list:");
		ArrayList<String> sorted = orderThis(lines, 0, lines.size());
		
		//System.out.println("\nThis should be sorted largest to smallest:");
		Iterator s = sorted.iterator();
		int count = 0;
		while(s.hasNext() && count < number)
		{
			count ++;
			System.out.println(s.next());
		}
		
		
		
	} //end of main (String args[])
	
	
	public static ArrayList<String> orderThis(ArrayList<String> theList, int start, int size)
	{//ordering by recursive selection sort
		//<editor-fold>
		//System.out.println("Call #" + (start+1));
		
		if (start == size)
			return theList;
		else if (start > size)
			return theList;
		else {
			String largest = theList.get(start);
			String temp;
			int indexOfLargest = start;
			for (int i = start; i < size ; i++)
			{
				temp = theList.get(i);
				if (temp.length() > largest.length())
				{
					largest = temp;
					indexOfLargest = i;
				}
			}
			//we have found the largest string and stored it in "largest"
			//if the largest item is already in the first spot, then move on
			if(theList.get(start).toString().length() < largest.toString().length())
			{
				//we need to swap the two
				temp = theList.get(start);
				theList.set(start, largest);
				theList.set(indexOfLargest, temp);
				
			}
			
		}
		//</editor-fold>
		return orderThis(theList, start+1, size);
	}
	
	

	
}
