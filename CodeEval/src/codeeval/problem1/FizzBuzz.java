/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Jason
 */
public class FizzBuzz {
	public static void main (String args[]) throws IOException
	{
		
		File file = new File(args[0]);
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String line;
        
		//loop on the input file
		while ((line = buffer.readLine()) != null) 
		{
            // Process line of input Here
			
			String[] inputs = line.trim().split(" ");
			int first = Integer.parseInt(inputs[0]);
			int second = Integer.parseInt(inputs[1]);
			int limit = Integer.parseInt(inputs[2]);
			
			//System.out.println("First: " + first + " | Second: " + second + " | Ending At: " + limit);
			String response = "1";
			
			for (int i = 2; i<= limit ; i++)
			{
				Boolean modFirst = false;
				Boolean modSecond = false;

				if(i % first == 0) modFirst = true;
				if(i % second == 0) modSecond = true;
				
				if(modFirst && modSecond)
					response = response + " FB";
				else if (modFirst && !modSecond)
					response = response + " F";
				else if (!modFirst && modSecond)
					response = response + " B";
				else
					response = response + " " + i;
			}
			System.out.println(response);
			
		}// end of file IO loop
		buffer.close();
	} // end of main (String args[])
	
}
